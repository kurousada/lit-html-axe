import { html, TemplateResult } from "lit-html";
/**
 * @private
 */
const isString = (x) => typeof x === "string" ||
    (typeof x === "object" &&
        x.constructor.name.slice(8, -1).toLowerCase() === "string");
/**
 * @private
 */
const isAttrs = (x) => typeof x === "object";
/**
 * @private
 */
const isContent = (x) => x instanceof TemplateResult || typeof x === "string" || x instanceof String;
/**
 * @private
 */
const isContentArray = (x) => Array.isArray(x) && x.every(v => isContent(v));
export function h(name_attrs_content, attrs_content, content) {
    let name = "div";
    let attrs = {};
    let contents = [];
    // check name_props_content
    if (isString(name_attrs_content)) {
        name = name_attrs_content;
    }
    else if (isContentArray(name_attrs_content)) {
        contents = name_attrs_content;
    }
    else if (isContent(name_attrs_content)) {
        contents = [name_attrs_content];
    }
    else if (isAttrs(name_attrs_content)) {
        attrs = name_attrs_content;
    }
    // check props_content
    if (isContentArray(attrs_content)) {
        contents = attrs_content;
    }
    else if (isContent(attrs_content)) {
        contents = [attrs_content];
    }
    else if (isAttrs(attrs_content)) {
        attrs = attrs_content;
    }
    // check content
    if (isContentArray(content)) {
        contents = content;
    }
    else if (isContent(content)) {
        contents = [content];
    }
    /*
    return html`<${name}${Object.entries(attrs)
      .map(([attr, value]) => ` ${attr}="${value}"`)
      .join("")}>${contents.join("")}</${name}>`;
    */
    const values = [];
    const texts = [`<${name}`];
    for (const [attr, value] of Object.entries(attrs)) {
        texts[texts.length - 1] += ` ${attr}="`;
        values.push(value);
        texts.push(`"`);
    }
    texts[texts.length - 1] += ">";
    for (const content of contents) {
        values.push(content);
        texts.push("");
    }
    texts[texts.length - 1] += `</${name}>`;
    const TemplateStringsArrayOf = (texts) => {
        texts.raw = texts;
        return texts;
    };
    return html(TemplateStringsArrayOf(texts), ...values);
}
/**
 * @private
 * Factory function of tag function.
 * This partially applies tag name with [[h]].
 * @param name tag name
 * @returns function to create tag, which type is [[f]]
 * @example
 * ```typescript
 * const p = h_of("p");
 * const render = () =>
 *   p({ id: "text", class: "gothic-font" }, "This is a content of my original element.");
 * ```
 */
const h_of = (name) => (attrs_content, content) => {
    // In order to avoid type errors, we can't simply return `h.bind(h, name)`
    if (isContent(attrs_content) || isContentArray(attrs_content)) {
        return h(name, attrs_content);
    }
    else if (isAttrs(attrs_content)) {
        return h(name, attrs_content, content);
    }
    else {
        return h(name);
    }
};
/*
 * Design decisions:
 *
 *   1. x() should be able to use as `x("p", x("br")())()`
 *   2. x() should accept both single tag name and multipule ones
 *   3. the return value of x() should be consistent between `x(['a', 'b', 'c'])` and `x(['a'])`
 *   4. when undefined is given, x() should return `h_of("div")`
 *
 * Thus we prefer
 *
 *   ```
 *   // this should be avoided to ensure consistency of the type of return value
 *   const a = (tags: string[]): f | { [key in string]: f } => x(...tags);
 *   ```
 *
 * to
 *
 *   ```
 *   // consistent and preferable
 *   const b = (tags: string[]): { [key in string]: f } => x(tags);
 *   ```
 */
export function x(tag_tags) {
    if (isString(tag_tags)) {
        return h_of(tag_tags);
    }
    else if (Array.isArray(tag_tags)) {
        const tags = tag_tags;
        return tags
            .map(tag => ({
            [tag]: h_of(tag)
        }))
            .reduce((obj, tagx) => Object.assign(obj, tagx), {});
    }
    else {
        return h_of();
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGl0LWh0bWwtYXhlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vc3JjL2xpdC1odG1sLWF4ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSxNQUFNLFVBQVUsQ0FBQztBQUVoRDs7R0FFRztBQUNILE1BQU0sUUFBUSxHQUFHLENBQUMsQ0FBVSxFQUFlLEVBQUUsQ0FDM0MsT0FBTyxDQUFDLEtBQUssUUFBUTtJQUNyQixDQUFDLE9BQU8sQ0FBQyxLQUFLLFFBQVE7UUFDbkIsQ0FBWSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxLQUFLLFFBQVEsQ0FBQyxDQUFDO0FBTTVFOztHQUVHO0FBQ0gsTUFBTSxPQUFPLEdBQUcsQ0FBQyxDQUFVLEVBQWMsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLFFBQVEsQ0FBQztBQUlsRTs7R0FFRztBQUNILE1BQU0sU0FBUyxHQUFHLENBQUMsQ0FBVSxFQUFnQixFQUFFLENBQzdDLENBQUMsWUFBWSxjQUFjLElBQUksT0FBTyxDQUFDLEtBQUssUUFBUSxJQUFJLENBQUMsWUFBWSxNQUFNLENBQUM7QUFFOUU7O0dBRUc7QUFDSCxNQUFNLGNBQWMsR0FBRyxDQUFDLENBQVUsRUFBa0IsRUFBRSxDQUNwRCxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztBQW1DakQsTUFBTSxVQUFVLENBQUMsQ0FDZixrQkFBeUQsRUFDekQsYUFBMkMsRUFDM0MsT0FBNkI7SUFFN0IsSUFBSSxJQUFJLEdBQUcsS0FBSyxDQUFDO0lBQ2pCLElBQUksS0FBSyxHQUFVLEVBQUUsQ0FBQztJQUN0QixJQUFJLFFBQVEsR0FBYyxFQUFFLENBQUM7SUFFN0IsMkJBQTJCO0lBQzNCLElBQUksUUFBUSxDQUFDLGtCQUFrQixDQUFDLEVBQUU7UUFDaEMsSUFBSSxHQUFHLGtCQUFrQixDQUFDO0tBQzNCO1NBQU0sSUFBSSxjQUFjLENBQUMsa0JBQWtCLENBQUMsRUFBRTtRQUM3QyxRQUFRLEdBQUcsa0JBQWtCLENBQUM7S0FDL0I7U0FBTSxJQUFJLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFO1FBQ3hDLFFBQVEsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7S0FDakM7U0FBTSxJQUFJLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxFQUFFO1FBQ3RDLEtBQUssR0FBRyxrQkFBa0IsQ0FBQztLQUM1QjtJQUVELHNCQUFzQjtJQUN0QixJQUFJLGNBQWMsQ0FBQyxhQUFhLENBQUMsRUFBRTtRQUNqQyxRQUFRLEdBQUcsYUFBYSxDQUFDO0tBQzFCO1NBQU0sSUFBSSxTQUFTLENBQUMsYUFBYSxDQUFDLEVBQUU7UUFDbkMsUUFBUSxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7S0FDNUI7U0FBTSxJQUFJLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRTtRQUNqQyxLQUFLLEdBQUcsYUFBYSxDQUFDO0tBQ3ZCO0lBRUQsZ0JBQWdCO0lBQ2hCLElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1FBQzNCLFFBQVEsR0FBRyxPQUFPLENBQUM7S0FDcEI7U0FBTSxJQUFJLFNBQVMsQ0FBQyxPQUFPLENBQUMsRUFBRTtRQUM3QixRQUFRLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztLQUN0QjtJQUVEOzs7O01BSUU7SUFFRixNQUFNLE1BQU0sR0FBRyxFQUFFLENBQUM7SUFDbEIsTUFBTSxLQUFLLEdBQUcsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDLENBQUM7SUFDM0IsS0FBSyxNQUFNLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJLE1BQU0sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7UUFDakQsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQztRQUN4QyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ25CLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7S0FDakI7SUFDRCxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsSUFBSSxHQUFHLENBQUM7SUFDL0IsS0FBSyxNQUFNLE9BQU8sSUFBSSxRQUFRLEVBQUU7UUFDOUIsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNyQixLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0tBQ2hCO0lBQ0QsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxJQUFJLEdBQUcsQ0FBQztJQUN4QyxNQUFNLHNCQUFzQixHQUFHLENBQUMsS0FBZSxFQUF3QixFQUFFO1FBQ3BFLEtBQTBDLENBQUMsR0FBZ0IsR0FBRyxLQUFLLENBQUM7UUFDdkUsT0FBUSxLQUF5QyxDQUFDO0lBQ3BELENBQUMsQ0FBQztJQUNGLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxFQUFFLEdBQUcsTUFBTSxDQUFDLENBQUM7QUFDeEQsQ0FBQztBQU9EOzs7Ozs7Ozs7Ozs7R0FZRztBQUNILE1BQU0sSUFBSSxHQUFHLENBQUMsSUFBYSxFQUFFLEVBQUUsQ0FBQyxDQUM5QixhQUEyQyxFQUMzQyxPQUE2QixFQUNiLEVBQUU7SUFDbEIsMEVBQTBFO0lBQzFFLElBQUksU0FBUyxDQUFDLGFBQWEsQ0FBQyxJQUFJLGNBQWMsQ0FBQyxhQUFhLENBQUMsRUFBRTtRQUM3RCxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLENBQUM7S0FDL0I7U0FBTSxJQUFJLE9BQU8sQ0FBQyxhQUFhLENBQUMsRUFBRTtRQUNqQyxPQUFPLENBQUMsQ0FBQyxJQUFJLEVBQUUsYUFBYSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0tBQ3hDO1NBQU07UUFDTCxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztLQUNoQjtBQUNILENBQUMsQ0FBQztBQThCRjs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBcUJHO0FBQ0gsTUFBTSxVQUFVLENBQUMsQ0FDZixRQUFxQztJQUVyQyxJQUFJLFFBQVEsQ0FBQyxRQUFRLENBQUMsRUFBRTtRQUN0QixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztLQUN2QjtTQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRTtRQUNsQyxNQUFNLElBQUksR0FBc0IsUUFBUSxDQUFDO1FBQ3pDLE9BQU8sSUFBSTthQUNSLEdBQUcsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDWCxDQUFDLEdBQUcsQ0FBQyxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUM7U0FDakIsQ0FBQyxDQUFDO2FBQ0YsTUFBTSxDQUFDLENBQUMsR0FBRyxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUM7S0FDeEQ7U0FBTTtRQUNMLE9BQU8sSUFBSSxFQUFFLENBQUM7S0FDZjtBQUNILENBQUMifQ==