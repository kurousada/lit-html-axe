import { TemplateResult } from "lit-html";
export declare type Attrs = {
    [key in string]: unknown;
};
export declare type Content = TemplateResult | string;
export interface Config {
    isVoid: boolean;
}
/**
 * Factory function of tag
 * @param name tag name (default: "div")
 * @param attrs name-value object of attributes a.k.a. [[Attrs]]
 * @param content [[Content]]
 */
export declare function h(name?: string, attrs?: Attrs, content?: Content | Content[]): TemplateResult;
/**
 * Factory function of tag
 * @param name tag name (default: "div")
 * @param content [[Content]]
 */
export declare function h(name?: string, content?: Content | Content[]): TemplateResult;
/**
 * Factory function of "div" tag
 * @param attrs name-value object of attributes a.k.a. [[Attrs]]
 * @param content [[Content]]
 * Note that it's preferred to create a custom element when you use div tag with specific class or id number of times
 */
export declare function h(attrs?: Attrs, content?: Content | Content[]): TemplateResult;
/**
 * Factory function of "div" tag
 * @param content [[Content]]
 */
export declare function h(content?: Content | Content[]): TemplateResult;
export declare type f = (attrs_content?: Attrs | Content | Content[], content?: Content | Content[]) => TemplateResult;
/**
 * Factory function of tag function.
 * This partially applies tag name with [[h]].
 * @param tag tag name
 * @returns function to create tag, which type is [[f]]
 * @example
 * ```typescript
 * const p = x("p");
 * const render = () =>
 *   p({ id: "text", class: "gothic-font" }, "This is a content of my original element.");
 * ```
 */
export declare function x(tag?: string): f;
/**
 * Utility function to create tag function from list of tag name.
 * @param tags list of tag name
 * @returns mapping object which key is tag name and value is a [[f]] function to create tag
 * @example
 * ```typescript
 * const { main, p, br } = x(["main", "p", "br"]);
 * const render = () =>
 *   main({ id: "main" }, p([
 *     "This is a content of my original element.",
 *     br()
 *   ]));
 * ```
 */
export declare function x(tags: readonly string[]): {
    [key in string]: f;
};
