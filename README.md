
# lit-html-axe

*Not ready for production yet. API should change.*

Write html tags as function instead of string with [lit-html](https://github.com/polymer/lit-html).

Inspired by [@hyperapp/html](https://github.com/Swizz/hyperapp-html) and [hyperaxe](https://github.com/ungoldman/hyperaxe).

## Example

Using [lit-element](https://github.com/polymer/lit-element), [html-tags](https://github.com/sindresorhus/html-tags) and [classnames](https://github.com/JedWatson/classnames):

```typescript
import { LitElement, customElement, property, css } from "lit-element";
import htmlTags from "html-tags";
import htmlVoidTags from "html-tags/void";
import classnames from "classnames";
import { h, x } from "lit-html-axe";

// axe-nize html tags
const { main, header, h1, p, br, input } = x([...htmlTags, ...htmlVoidTags]);
// axe-nize cutom tag
const awesome = x('my-awesome-element');

@customElement("test-element")
export class TestElement extends LitElement {
  @property()
  title = "This is a test.";
  @property()
  username = "guest";

  static get styles() {
    const color = css`green`;
    return css`
      h1 {
        color: ${color};
      }
    `;
  }

  render() {
    // ----------------------------------------------------------------------
    return main({ id: "main", "class": classnames("main", "container") }, [
      header(h1(this.title)),
      p([
        h("span", `Hello, ${this.username}.`),
        br(),
        input({ type: "text", value: this.username, "@input": e => {
          const value = e.target.value;
          if (value != null && value !== "") {
            this.username = value;
          }
        } }),
        input({ type: "checkbox", "?checked": this.username === "guest" }, "default name"),
        br(),
        awesome({ "class": classnames("awesome") }, "My Awesome Element is HERE!") /* any custom tag is available */
      ])
    ]);
    // ----------------------------------------------------------------------
  }
}
```

## Pros

- Compatible with `h()` of [hyperapp](https://github.com/hyperapp/hyperapp)
- Using `x()`, everything becomes function and thus easy to compose or wrap them up
- Editor support for highlighting, word-completion and type-hints

## Cons

- Not compatible with [hyperscript](https://github.com/hyperhype/hyperscript) yet
- See below for known performance issues

## Installation

*Not ready for production yet. API should change*

```sh
yarn add lit-html-axe
```

## Usage

```typescript
import { axe, x } from "lit-element-axe";


// use x() with any tag name
const p = x("p");

// default is div
const div = x();

// or pass list of tag names
const { main, h1, input } = x(["main", "h1", "input"]);

// rename it when tag name is too long or a keyword
const {
  button: btn,
  "var": variable,
  "my-some-useful-custom-element": mine
} = x(["var", "button", "my-some-useful-custom-element"])

// if the argument is empty list, it returns {}
x([]) // -> {}

// pass content
p("Here goes content."); // -> html`<p>Here goes content.</p>`

// or list of contents
p([
  "Hi, there.",
  br(),
  "This is a content."
]);
// -> html`<p>Hi, there.<br>This is a content.</p>`

// specify attributes as object
h1({ id: "title", class: "header-title" }, "Title");
// -> html`<h1 id="title" class="header-title">Title</h1>`

// list of contents are also worked
p({ class: "text" }, [
  "Lorem ipsum",
  br(),
  "... and I don't know the rest."
]);
// -> html`<p class="text">Lorem ipsum<br>... and I don't know the rest.</p>`


// a bit complex one
main({ id: "main", class: "container" }, [
  h1({ id: "title", class: "container__title" }, "Example"),
  div({ class: "container__content" }, [
    p({ id: "status", class: "container__content--status" }, [
      "Button pressed?",
      html`<br>`
      input({ type: "text", "value": main_btn_state ? "clicked" : "not clicked" })
    ]),
    btn({ id: "main_btn", "@click": main_btn_clicked }, "Click me!"),
    mine(variable("some_variable"))
  ])
]);
```

## Documentation

Generate with following command and see `docs/`.

```sh
yarn run doc
```

Or, visit https://kurousada.gitlab.io/lit-html-axe.

## For Developpers

We recommend to use Linux/Mac because some npm scripts are written in Bash.

### Installation

```sh
git clone https://gitlab.com/kurousada/lit-html-axe
cd lit-element-axe
yarn install --production=false
```

### Test

*Not yet prepared*

```sh
yarn run test
```

### Build

```sh
yarn run build  # see `build/` then
```

## Performance issues

`html()` of lit-html caches DOM based on given values.
However we have no way to know which to cache in `h()` of lit-html-axe because everything becomes string before given to it.

Thus, every value of all attributes and every contents will be cached.
Also when a text contains a text binding, whole text will be cached instead of the text binding itself.

For example:

```typescript
const v = "variable text";

const render = () =>
  h("p", [
    `value is ${v}`,
    h("input", { "type": "text", ".value": v })
  ]);
```

is equivalent to

```typescript
const v = "variable text";

// Even static string will be cached although they won't change
// and whole text containing text binding will be cached too
const render = () =>
  html`<p>${
    "value is variable text"
  }${
    html`<input type="${"text"}" .value="${v}"></input>`
  }</p>`;
```

So it's better to write

```typescript
const v = "variable text";

const render = () =>
  h("p", [
    `value is `,
    v,
    h("input", { "type": "text", ".value": v })
  ]);
```

We haven't measure how much it costs exactly, but it's recommended to use `html()` when you need efficient caching.

This will be solved (or at least improved) in the future.

## License

MIT License (see `LICENSE.txt`)
