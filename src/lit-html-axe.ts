import { html, TemplateResult } from "lit-html";

/**
 * @private
 */
const isString = (x: unknown): x is string =>
  typeof x === "string" ||
  (typeof x === "object" &&
    (x as object).constructor.name.slice(8, -1).toLowerCase() === "string");

export type Attrs = {
  [key in string]: unknown;
};

/**
 * @private
 */
const isAttrs = (x: unknown): x is Attrs => typeof x === "object";

export type Content = TemplateResult | string;

/**
 * @private
 */
const isContent = (x: unknown): x is Content =>
  x instanceof TemplateResult || typeof x === "string" || x instanceof String;

/**
 * @private
 */
const isContentArray = (x: unknown): x is Content[] =>
  Array.isArray(x) && x.every(v => isContent(v));

export interface Config {
  isVoid: boolean;
}

/**
 * Factory function of tag
 * @param name tag name (default: "div")
 * @param attrs name-value object of attributes a.k.a. [[Attrs]]
 * @param content [[Content]]
 */
export function h(
  name?: string,
  attrs?: Attrs,
  content?: Content | Content[]
): TemplateResult;
/**
 * Factory function of tag
 * @param name tag name (default: "div")
 * @param content [[Content]]
 */
export function h(name?: string, content?: Content | Content[]): TemplateResult;
/**
 * Factory function of "div" tag
 * @param attrs name-value object of attributes a.k.a. [[Attrs]]
 * @param content [[Content]]
 * Note that it's preferred to create a custom element when you use div tag with specific class or id number of times
 */
export function h(attrs?: Attrs, content?: Content | Content[]): TemplateResult;
/**
 * Factory function of "div" tag
 * @param content [[Content]]
 */
export function h(content?: Content | Content[]): TemplateResult;
export function h(
  name_attrs_content?: string | Attrs | Content | Content[],
  attrs_content?: Attrs | Content | Content[],
  content?: Content | Content[]
): TemplateResult {
  let name = "div";
  let attrs: Attrs = {};
  let contents: Content[] = [];

  // check name_props_content
  if (isString(name_attrs_content)) {
    name = name_attrs_content;
  } else if (isContentArray(name_attrs_content)) {
    contents = name_attrs_content;
  } else if (isContent(name_attrs_content)) {
    contents = [name_attrs_content];
  } else if (isAttrs(name_attrs_content)) {
    attrs = name_attrs_content;
  }

  // check props_content
  if (isContentArray(attrs_content)) {
    contents = attrs_content;
  } else if (isContent(attrs_content)) {
    contents = [attrs_content];
  } else if (isAttrs(attrs_content)) {
    attrs = attrs_content;
  }

  // check content
  if (isContentArray(content)) {
    contents = content;
  } else if (isContent(content)) {
    contents = [content];
  }

  /*
  return html`<${name}${Object.entries(attrs)
    .map(([attr, value]) => ` ${attr}="${value}"`)
    .join("")}>${contents.join("")}</${name}>`;
  */

  const values = [];
  const texts = [`<${name}`];
  for (const [attr, value] of Object.entries(attrs)) {
    texts[texts.length - 1] += ` ${attr}="`;
    values.push(value);
    texts.push(`"`);
  }
  texts[texts.length - 1] += ">";
  for (const content of contents) {
    values.push(content);
    texts.push("");
  }
  texts[texts.length - 1] += `</${name}>`;
  const TemplateStringsArrayOf = (texts: string[]): TemplateStringsArray => {
    (((texts as unknown) as TemplateStringsArray).raw as string[]) = texts;
    return (texts as unknown) as TemplateStringsArray;
  };
  return html(TemplateStringsArrayOf(texts), ...values);
}

export type f = (
  attrs_content?: Attrs | Content | Content[],
  content?: Content | Content[]
) => TemplateResult;

/**
 * @private
 * Factory function of tag function.
 * This partially applies tag name with [[h]].
 * @param name tag name
 * @returns function to create tag, which type is [[f]]
 * @example
 * ```typescript
 * const p = h_of("p");
 * const render = () =>
 *   p({ id: "text", class: "gothic-font" }, "This is a content of my original element.");
 * ```
 */
const h_of = (name?: string) => (
  attrs_content?: Attrs | Content | Content[],
  content?: Content | Content[]
): TemplateResult => {
  // In order to avoid type errors, we can't simply return `h.bind(h, name)`
  if (isContent(attrs_content) || isContentArray(attrs_content)) {
    return h(name, attrs_content);
  } else if (isAttrs(attrs_content)) {
    return h(name, attrs_content, content);
  } else {
    return h(name);
  }
};

/**
 * Factory function of tag function.
 * This partially applies tag name with [[h]].
 * @param tag tag name
 * @returns function to create tag, which type is [[f]]
 * @example
 * ```typescript
 * const p = x("p");
 * const render = () =>
 *   p({ id: "text", class: "gothic-font" }, "This is a content of my original element.");
 * ```
 */
export function x(tag?: string): f;
/**
 * Utility function to create tag function from list of tag name.
 * @param tags list of tag name
 * @returns mapping object which key is tag name and value is a [[f]] function to create tag
 * @example
 * ```typescript
 * const { main, p, br } = x(["main", "p", "br"]);
 * const render = () =>
 *   main({ id: "main" }, p([
 *     "This is a content of my original element.",
 *     br()
 *   ]));
 * ```
 */
export function x(tags: readonly string[]): { [key in string]: f };
/*
 * Design decisions:
 *
 *   1. x() should be able to use as `x("p", x("br")())()`
 *   2. x() should accept both single tag name and multipule ones
 *   3. the return value of x() should be consistent between `x(['a', 'b', 'c'])` and `x(['a'])`
 *   4. when undefined is given, x() should return `h_of("div")`
 *
 * Thus we prefer
 *
 *   ```
 *   // this should be avoided to ensure consistency of the type of return value
 *   const a = (tags: string[]): f | { [key in string]: f } => x(...tags);
 *   ```
 *
 * to
 *
 *   ```
 *   // consistent and preferable
 *   const b = (tags: string[]): { [key in string]: f } => x(tags);
 *   ```
 */
export function x(
  tag_tags?: string | readonly string[]
): f | { [key in string]: f } {
  if (isString(tag_tags)) {
    return h_of(tag_tags);
  } else if (Array.isArray(tag_tags)) {
    const tags: readonly string[] = tag_tags;
    return tags
      .map(tag => ({
        [tag]: h_of(tag)
      }))
      .reduce((obj, tagx) => Object.assign(obj, tagx), {});
  } else {
    return h_of();
  }
}
