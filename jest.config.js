module.exports = {
  collectCoverage: true,
  coverageDirectory: "<rootDir>/coverage",
  coveragePathIgnorePatterns: [
    "/node_modules/",
    "<rootDir>/build/",
    "<rootDir>/docs"
  ],
  transform: {
    "^.+\\.tsx?$": "ts-jest"
  },
  testPathIgnorePatterns: [
    "/node_modules/",
    "<rootDir>/build/",
    "<rootDir>/docs"
  ],
  testRegex: "/tests?/.*\\.(test|spec)\\.[jt]sx?$",
  moduleFileExtensions: ["ts", "js", "json"],
  globals: {
    "ts-jest": {
      tsConfig: "tsconfig.json"
    }
  }
};
