/*
 * Because of some issues, we cannot import lit-html, thus this test can't run...
 *
 * https://stackoverflow.com/questions/58613492/how-to-resolve-cannot-use-import-statement-outside-a-module-in-jest
 */

import { html, TemplateResult } from "lit-html";
import { h, x } from "../src/lit-html-axe";

describe("x", () => {
  it("returns <br />", () => {
    expect(x("br")().getHTML()).toBe("<br />");
  });
  it("returns TemplateResult", () => {
    expect(x("br")).toBeInstanceOf(TemplateResult);
  });
});

describe("h", () => {
  it("returns TemplateResult", () => {
    expect(h("br")).toBeInstanceOf(TemplateResult);
  });
  it('returns html`<p id="text">test</p>`', () => {
    expect(h("p", { id: "text" }, ["test"])).toMatchObject(
      html`
        <p id="text">test</p>
      `
    );
  });
  it("returns <br />", () => {
    expect(h("br").getHTML()).toBe("<br />");
  });
  it('returns <p id="text">test</p>', () => {
    expect(h("p", { id: "text" }, ["test"]).getHTML()).toBe(
      '<p id="text">test</p>'
    );
  });
});
